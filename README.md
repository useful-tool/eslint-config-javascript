#### 如果要自动设置配置，请在项目目录中执行以下步骤：

```
npx eslint --init
```

* 选择“使用流行的样式指南”。
* 选择“标准”。
* 选择配置文件格式。
* 如果出现提示，请确认必要的依赖项的安装。
* 以上步骤将自动设置ESLint配置并为您安装必要的依赖项。

###### 如果要手动设置配置，请运行以下命令(注意不要漏了babel-eslint)：

``` shell
npm install --save-dev eslint-config-standard eslint-plugin-standard eslint-plugin-promise eslint-plugin-import eslint-plugin-node babel-eslint
```

###### 然后，安装eslint-config-javacript

```
npm install eslint-config-javascript -D
```

###### 然后，修改.eslintrc的继承：

``` json
{
  "extends": "eslint-config-javascript"
}
```

##### eslint-config-javascript基本的javascript规范
```json
{
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "standard",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "parser": "babel-eslint",
    "rules": {
    "semi-style": [
            "error",
            "last"
        ],
        "comma-dangle": [
            "error",
            "only-multiline"
        ],
        "quotes": [
            "warn",
            "single"
        ],
        "object-curly-spacing": [
            "warn",
            "never"
        ],
        "array-bracket-spacing": [
            "warn",
            "never"
        ],
        "computed-property-spacing": [
            "warn",
            "never"
        ],
        "space-in-parens": [
            "warn",
            "never"
        ],
        "space-infix-ops": [
            "error"
        ],
        "space-before-function-paren": [
            "error",
            "never"
        ],
        "arrow-spacing": [
            "warn",
            {
                "before": true,
                "after": true
            }
        ],
        "eqeqeq": [
            "error",
            "always"
        ],
        "no-unused-vars": [
            "off",
            {
                "args": "none"
            }
        ],
        "key-spacing": "off",
        "max-len": [
            2,
            130,
            2
        ],
        "no-eval": "error",
        "semi": [
            2,
            "always"
        ],
        "no-array-constructor": "error",
        "no-cond-assign": "error",
        "no-extra-semi": "error",
        "no-extra-parens": [
            "error",
            "all",
            {
                "ignoreJSX": "multi-line"
            }
        ],
        "no-lonely-if": "warn",
        "complexity": [
            "error",
            12
        ]
    }
}
```
##### [更多规则配置](https://cn.eslint.org/docs/rules/)